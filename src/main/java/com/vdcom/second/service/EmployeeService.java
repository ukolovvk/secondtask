package com.vdcom.second.service;

import com.vdcom.second.entity.Employee;
import java.util.List;

public interface EmployeeService {

    List<Employee> getAllEmployee();

    void updateEmployee(Employee employee);

    void addEmployee(Employee employee);

    void deleteEmployeeById(int id);
}
