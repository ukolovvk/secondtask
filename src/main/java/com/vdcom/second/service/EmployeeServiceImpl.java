package com.vdcom.second.service;

import com.vdcom.second.entity.Employee;
import com.vdcom.second.repositories.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepo employeeRepo;

    public List<Employee> getAllEmployee() {
        return employeeRepo.findAll();
    }

    public void updateEmployee(Employee employee) {
        employeeRepo.save(employee);
    }

    public void addEmployee(Employee employee) {
        employeeRepo.save(employee);
    }

    public void deleteEmployeeById(int id) {
        employeeRepo.deleteById(id);
    }
}
