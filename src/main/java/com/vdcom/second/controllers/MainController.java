package com.vdcom.second.controllers;

import com.vdcom.second.entity.Employee;
import com.vdcom.second.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/vdcom")
public class MainController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employees")
    public String printAllEmployees(Model model) {
        model.addAttribute("employees", employeeService.getAllEmployee());
        return "index";
    }

    @PostMapping("/updateemployee")
    public String updateEmployee(
            @RequestParam(value = "empId", required = false) Integer id,
            @RequestParam(value = "firstName") String firstName,
            @RequestParam(value = "lastName") String lastName,
            @RequestParam(value = "phone") String phone,
            @RequestParam(value = "position") String position,
            @RequestParam(value = "salary") String salary,
            Model model
    ) {
        long resultSalary;
        try {
            resultSalary = Long.parseLong(salary);
        } catch (NumberFormatException ex) {
            model.addAttribute("employees", employeeService.getAllEmployee());
            return "index";
        }
        if (id == null)
            employeeService.addEmployee(new Employee(firstName, lastName, phone, position, resultSalary));
        else
            employeeService.updateEmployee(new Employee(id, firstName, lastName, phone, position, resultSalary));
        model.addAttribute("employees", employeeService.getAllEmployee());
        return "index";
    }

    @PostMapping("/deleteemployee/{id}")
    public String deleteEmployee(@PathVariable Integer id, Model model) {
        employeeService.deleteEmployeeById(id);
        model.addAttribute("employees", employeeService.getAllEmployee());
        return "index";
    }
}
