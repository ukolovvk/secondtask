INSERT INTO employees(id, first_name, last_name, phone, position, salary) VALUES (
    nextval('employees_id_seq'),
    'fname1',
    'lname1',
    '+79277075566',
    'Senior Software engineer',
    4000
);

INSERT INTO employees(id, first_name, last_name, phone, position, salary) VALUES (
     nextval('employees_id_seq'),
     'fname2',
     'lname2',
     '+7927707112',
     'Project manager',
     5000
 );

INSERT INTO employees(id, first_name, last_name, phone, position, salary) VALUES (
     nextval('employees_id_seq'),
     'fname3',
     'lname3',
     '+79271234433',
     'Software engineer',
     3000
 );

INSERT INTO employees(id, first_name, last_name, phone, position, salary) VALUES (
     nextval('employees_id_seq'),
     'fname4',
     'lname4',
     '+79279990099',
     'Tech Lead',
     4500
 );

INSERT INTO employees(id, first_name, last_name, phone, position, salary) VALUES (
     nextval('employees_id_seq'),
     'fname5',
     'lname5',
     '+79277653189',
     'Dev Ops',
     3500
 );