DROP TABLE IF EXISTS employees;
DROP SEQUENCE IF EXISTS employees_id_seq;

CREATE TABLE IF NOT EXISTS employees (
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    phone VARCHAR(20) NOT NULL UNIQUE,
    position VARCHAR(50) NOT NULL,
    salary BIGINT NOT NULL
);

CREATE SEQUENCE IF NOT EXISTS employees_id_seq START 1;