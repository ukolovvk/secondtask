from openjdk:17-jdk-alpine

MAINTAINER ukolovvk

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src

RUN ./mvnw clean package

ENTRYPOINT ["java", "-jar", "/target/SecondTask-1.0.jar"]
